/* GiLoad -- a game-file loader for Quixe
 * Designed by Andrew Plotkin <erkyrath@eblong.com>
 * <http://eblong.com/zarf/glulx/quixe/>
 *
 * 
 * This Javascript library is copyright 2010-2016 by Andrew Plotkin.
 * It is distributed under the MIT license; see the "LICENSE" file.
 *
 * This library loads a game image (by one of several possible methods)
 * and then starts up the display layer and game engine. It also extracts
 * data from a Blorb image, if that's what's provided. It is distributed
 * as part of the Quixe engine, but can also be used by IFVMS. Thus it is
 * equipped to handle both Glulx and Z-code games (naked or Blorbed).
 *
 * (This code makes use of the jQuery library, which therefore must be
 * available.)
 *
 * When you are putting together a Quixe installation page, you call
 * GiLoad.load_run() to get the game started. You should do this in the
 * document's "onload" handler, or later. (If you call it before "onload" 
 * time, it may not work.)
 *
 * You can do this in a couple of different ways:
 *
 * GiLoad.load_run(OPTIONS) -- load and run the game using the options
 *   passed as the argument. If OPTIONS is null or not provided, the
 *   global "game_options" object is considered. (The various options are
 *   described below.) This invocation assumes Glulx format.
 *
 * GiLoad.load_run(OPTIONS, IMAGE, IMAGEOPTIONS) -- run the game with the
 *   given options. The IMAGE argument, if not null, should be the game
 *   file itself (a glulx, zcode, or blorb file). The IMAGEOPTIONS describe
 *   how the game file is encoded. It should contain:
 *
 *   IMAGEOPTIONS.format: Describes how the game file is encoded:
 *     "base64": a base64-encoded binary file
 *     "raw": a binary file stored in a string
 *     "array": an array of (numeric) byte values
 *
 *   If the third argument is a string rather than an object, it is taken
 *   to be IMAGEOPTIONS.format.
 *
 *   If OPTIONS is null, the global "game_options" object is considered.
 *
 * These are the game options. Most have default values, so you only have
 * to declare the ones you want to change.
 *
 *   use_query_story: If this is true, you (or the player) can use a
 *     "?story=..." URL parameter to load any game file. If it is false,
 *     this parameter is ignored. (default: true)
 *   set_page_title: If true, the loader will change the document title
 *     to describe the game being loaded. If false, the document title
 *     will be left alone. (default: true)
 *   default_page_title: A default label for the game, if none could be
 *     extracted from the metadata or URL. (default: "Game")
 *   engine_name: Label used in the page title along with default_page_title.
 *     (default: "Quixe" or "IFVMS")
 *   default_story: The URL of the game file to load, if not otherwise
 *     provided.
 *   proxy_url: The URL of the web-app service which is used to convert
 *     binary data to Javascript, if the browser needs that. (default:
 *     https://zcode.appspot.com/proxy/)
 *   image_info_map: An object which describes all the available
 *     images, if they are provided as static URL data. (If this is not
 *     provided, we rely on Blorb resources.) This can be an object
 *     or a string; in the latter case, we look up a global object with
 *     that name.
 *   exit_warning: A message to display (in a blue warning pane) when
 *     the game exits. If empty or null, no message is displayed.
 *     (default: "The game session has ended.")
 *   do_vm_autosave: If set, the VM will check for a snapshot when
 *     launching, and load it if present. The VM will also save a snapshot
 *     after every move. (default: false)
 *   clear_vm_autosave: If set, the VM will clear any snapshot at launch
 *     (so will not load one even if do_vm_autosave is set). (default:
 *     false)
 *   game_format_name: Label used in loading error messages. (default:
 *     "Glulx" for Quixe, "" otherwise)
 *   blorb_gamechunk_type: Chunk type to extract from a Blorb file.
 *     (default: "GLUL" for Quixe, null otherwise)
 *   vm: The game engine interface object. (default: Quixe)
 *   io: The display layer interface object. (default: Glk)
 *   
 *   You can also include any of the display options used by the GlkOte
 *   library, such as gameport, windowport, spacing, ...
 *   And also the interpreter options used by the Quixe library, such as
 *   rethrow_exceptions, ...
 *
 *   For backwards compatibility, if options.vm is omitted or is the
 *   windows.Quixe object, then several other options (engine_name,
 *   blorb_gamechunk_type, game_format_name) are set up with values
 *   appropriate for Glulx game files.
 *
 * GiLoad.find_data_chunk(NUM) -- this finds the Data chunk of the
 *   given number from the Blorb file. The returned object looks like
 *   { data:[...], type:"..." } (where the type is TEXT or BINA).
 *   If there was no such chunk, or if the game was loaded from a non-
 *   Blorb file, this returns undefined.
 *
 * GiLoad.get_metadata(FIELD) -- this returns a metadata field (a
 *   string) from the iFiction <bibliographic> section. If there is
 *   no such field, or if the game was loaded from a non-Blorb
 *   file, this returns undefined.
 *
 * GiLoad.get_image_info(NUM) -- returns an object describing an image,
 *   or undefined.
 *
 * GiLoad.get_debug_info() -- returns an array containing debug info,
 *   or null.
 *
 * GiLoad.get_image_url(NUM) -- returns a URL describing an image, or
 *   undefined.
 */

/* Nuixe changes:
- Export the closure instead of executing it.
- Remove custom implementation of base64 function that were here only for Internet Explorer.
- Remove a lot of options.
- Remove get_query_params, decode_raw_text.
- Modified start_game.
*/

/* Put everything inside the GiLoad namespace. */
export default function() {

// NUIXE ADDED: Function to replace `GlkOte.log`
function qlog(msg) {
    console.log("[GiLoad] " + msg);
}

// NUIXE CHANGED: Removed all unused options, because they don't concern Nuixe or because we already know their values (for example, `vm` will always be a Quixe instance.).
/* Start with the defaults. These can be modified later by the game_options
   defined in the HTML file.

   Note that the "vm" and "io" entries are not filled in here, because
   we don't know whether the Quixe or Glk libraries were loaded before
   this one. We'll fill them in at load_run() time.
*/
var all_options = {
    game_format_name: 'Glulx',  // used in error messages
    image_info_map: null,  // look for images in Blorb data,
    engine_name:  'Quixe',
    blorb_gamechunk_type: 'GLUL',
    exit_warning: 'The game session has ended.'
};

// NUIXE ADDED: Replaces the io property of all_options.
var Glk = null;

// NUIXE ADDED: Replaces the vm property of all_options.
var VM = null;

// NUIXE ADDED: Used when initing Glk.
var GiDispa = null;

var gameurl = null;  /* The URL we are loading. */
var metadata = {}; /* Title, author, etc -- loaded from Blorb */
var debug_info = null; /* gameinfo.dbg file -- loaded from Blorb */
var blorbchunks = {}; /* Indexed by "USE:NUMBER" -- loaded from Blorb */
var alttexts = {}; /* Indexed by "USE:NUMBER" -- loaded from Blorb */


// NUIXE CHANGED: Greatly simplified since we know what will be the options
/* Begin the loading process. This is what you call to start a game.
*/
function load_run(image, optobj) {
    Glk = optobj.glk;
    VM = optobj.vm;
    GiDispa = optobj.gi_dispa;
    // We assume the image is base64-encoded.
    image = decode_base64(image);
    start_game(image, {
        gi_load: optobj.gi_load,
        on_update: optobj.on_update,
        on_error: optobj.on_error,
        on_warning: optobj.on_warning
    });
}

// NUIXE CHANGED: Make this function do nothing. I don't think it's used anyway, without GlkOte.
/* I learned this terrible trick for turning a relative URL absolute. 
   It's supposed to work on all browsers, if you don't go mad.
   (This uses DOM methods rather than jQuery.)
*/
function absolutize(url) {
    return url;
    // /* I don't know if this is slow (or safe) for data URLs. Might as
    //    well skip out of the easy cases first, anyhow. */
    // if (url.match(/^(file|data|http|https):/i)) {
    //     return url;
    // }

    // var div = document.createElement('div');
    // div.innerHTML = '<a></a>';
    // div.firstChild.href = url;
    // div.innerHTML = div.innerHTML;
    // return div.firstChild.href;
}

/* Return a metadata field, or undefined if there is no such field
   (or if no metadata was loaded).
*/
function get_metadata(val) {
    return metadata[val];
}

/* Return the gameinfo.dbg file (as an array of bytes), if it was
   loaded.
*/
function get_debug_info() {
    return debug_info;
}

/* Return information describing an image. This might be loaded from static
   data or from a Blorb file.
   
   The return value will be null or an object:
   { image:VAL, type:STRING, alttext:STRING, width:NUMBER, height:NUMBER }

   (The alttext and type may be absent if not supplied.)
*/
function get_image_info(val) {
    if (all_options.image_info_map != undefined) {
        var img = all_options.image_info_map[val];
        if (img)
            return img;
    }

    var chunk = blorbchunks['Pict:'+val];
    if (chunk) {
        var img = { image:val };
        if (chunk.type == 'JPEG')
            img.type = 'jpeg';
        else if (chunk.type == 'PNG ')
            img.type = 'png';
        else
            img.type = '????';

        /* Extract the image size, if we don't have it cached already.
           We could do this by creating an Image DOM element and measuring
           it, but that could be slow. Instead, we'll parse the PNG or
           JPEG data directly. It's easier than it sounds! */
        if (chunk.imagesize === undefined) {
            var imgsize = undefined;
            if (chunk.type == 'JPEG') {
                imgsize = find_dimensions_jpeg(chunk.content);
            }
            else if (chunk.type == 'PNG ') {
                imgsize = find_dimensions_png(chunk.content);
            }
            if (imgsize)
                chunk.imagesize = imgsize;
        }
        if (chunk.imagesize) {
            img.width = chunk.imagesize.width;
            img.height = chunk.imagesize.height;
        }

        /* Extract the alt-text, if available. */
        var rdtext = alttexts['Pict:'+val];
        if (rdtext)
            img.alttext = rdtext;
        return img;
    }

    return undefined;
}

/* Return a URL representing an image. This might be loaded from static
   data or from a Blorb file.

   The return value will be null or a URL. It might be a "data:..." URL.
*/
function get_image_url(val) {
    if (all_options.image_info_map) {
        var img = all_options.image_info_map[val];
        if (img && img.url)
            return absolutize(img.url);
    }

    var chunk = blorbchunks['Pict:'+val];
    if (chunk) {
        if (chunk.dataurl)
            return chunk.dataurl;

        var info = get_image_info(val);
        if (info && chunk.content) {
            var mimetype = 'application/octet-stream';
            if (chunk.type == 'JPEG')
                mimetype = 'image/jpeg';
            else if (chunk.type == 'PNG ')
                mimetype = 'image/png';
            var b64dat = encode_base64(chunk.content);
            chunk.dataurl = 'data:'+mimetype+';base64,'+b64dat;
            return chunk.dataurl;
        }
    }

    return undefined;
}

/* Return the Data chunk with the given number, or undefined if there
   is no such chunk. (This is used by the glk_stream_open_resource()
   functions.)
*/
function find_data_chunk(val) {
    var chunk = blorbchunks['Data:'+val];
    if (!chunk)
        return null;

    var returntype = chunk.type;
    if (returntype == 'FORM')
        returntype = 'BINA';

    return { data:chunk.content, type:returntype };
}

/* Look through a Blorb file (provided as a byte array) and return the
   game file chunk (ditto). If no such chunk is found, returns null.
   The gamechunktype argument should be 'ZCOD' or 'GLUL'.

   This also loads the IFID metadata into the metadata object, and
   caches DATA chunks where we can reach them later.
*/
function unpack_blorb(image, gamechunktype) {
    var len = image.length;
    var ix;
    var rindex = [];
    var result = null;
    var pos = 12;

    while (pos < len) {
        var chunktype = String.fromCharCode(image[pos+0], image[pos+1], image[pos+2], image[pos+3]);
        pos += 4;
        var chunklen = (image[pos+0] << 24) | (image[pos+1] << 16) | (image[pos+2] << 8) | (image[pos+3]);
        pos += 4;

        if (chunktype == "RIdx") {
            var npos = pos;
            var numchunks = (image[npos+0] << 24) | (image[npos+1] << 16) | (image[npos+2] << 8) | (image[npos+3]);
            npos += 4;
            for (ix=0; ix<numchunks; ix++) {
                var chunkusage = String.fromCharCode(image[npos+0], image[npos+1], image[npos+2], image[npos+3]);
                npos += 4;
                var chunknum = (image[npos+0] << 24) | (image[npos+1] << 16) | (image[npos+2] << 8) | (image[npos+3]);
                npos += 4;
                var chunkpos = (image[npos+0] << 24) | (image[npos+1] << 16) | (image[npos+2] << 8) | (image[npos+3]);
                npos += 4;
                rindex.push( { usage:chunkusage, num:chunknum, pos:chunkpos } );
            }
        }
        if (chunktype == "IFmd") {
            var arr = image.slice(pos, pos+chunklen);
            var dat = encode_utf8_text(arr);
            var met = $('<metadata>').html(dat);
            var bibels = met.find('bibliographic').children();
            if (bibels.length) {
                var el;
                for (ix=0; ix<bibels.length; ix++) {
                    el = bibels[ix];
                    metadata[el.tagName.toLowerCase()] = el.textContent;
                }
            }
        }
        if (chunktype == "Dbug") {
            /* Because this is enormous, we only save it if the option
               is set to use it. */
            if (all_options.debug_info_chunk) {
                var arr = image.slice(pos, pos+chunklen);
                debug_info = arr;
            }
        }
        if (chunktype == "RDes") {
            var npos = pos;
            var numentries = (image[npos+0] << 24) | (image[npos+1] << 16) | (image[npos+2] << 8) | (image[npos+3]);
            npos += 4;
            for (ix=0; ix<numentries; ix++) {
                var rdusage = String.fromCharCode.apply(this, image.slice(npos, npos+4));
                npos += 4;
                var rdnumber = (image[npos+0] << 24) | (image[npos+1] << 16) | (image[npos+2] << 8) | (image[npos+3]);
                npos += 4;
                var rdlen = (image[npos+0] << 24) | (image[npos+1] << 16) | (image[npos+2] << 8) | (image[npos+3]);
                npos += 4;
                var rdtext = encode_utf8_text(image.slice(npos, npos+rdlen));
                npos += rdlen;
                alttexts[rdusage+':'+rdnumber] = rdtext;
            }
        }

        pos += chunklen;
        if (pos & 1)
            pos++;
    }

    /* We don't want to retain the original Blorb image in memory; it's
       enormous. We'll split out the addressable chunks (those with
       usages) and retain those individually. Still enormous, but less
       so.

       (It's probably a waste to save the cover image -- that probably
       won't ever be used by the game. But it might be.) 
    */

    for (ix=0; ix<rindex.length; ix++) {
        var el = rindex[ix];
        pos = el.pos;
        var chunktype = String.fromCharCode(image[pos+0], image[pos+1], image[pos+2], image[pos+3]);
        pos += 4;
        var chunklen = (image[pos+0] << 24) | (image[pos+1] << 16) | (image[pos+2] << 8) | (image[pos+3]);
        pos += 4;

        el.type = chunktype;
        el.len = chunklen;
        el.content = null;

        if (el.usage == "Exec" && el.num == 0 && chunktype == gamechunktype) {
            result = image.slice(pos, pos+chunklen);
        }
        else {
            if (chunktype == "FORM") {
                el.content = image.slice(pos-8, pos+chunklen);
            }
            else {
                el.content = image.slice(pos, pos+chunklen);
            }
            blorbchunks[el.usage+':'+el.num] = el;
        }
    }

    return result;
}

/* In the following functions, "decode" means turning native string data
   into an array of numbers; "encode" is the other direction. That's weird,
   I know. It's because an array of byte values is the natural data format
   of Glulx code.
*/

/* Convert an array of numeric byte values (containing UTF-8 encoded text)
   into a string.
*/
function encode_utf8_text(arr) {
    var res = [];
    var ch;
    var pos = 0;

    while (pos < arr.length) {
        var val0, val1, val2, val3;
        if (pos >= arr.length)
            break;
        val0 = arr[pos];
        pos++;
        if (val0 < 0x80) {
            ch = val0;
        }
        else {
            if (pos >= arr.length)
                break;
            val1 = arr[pos];
            pos++;
            if ((val1 & 0xC0) != 0x80)
                break;
            if ((val0 & 0xE0) == 0xC0) {
                ch = (val0 & 0x1F) << 6;
                ch |= (val1 & 0x3F);
            }
            else {
                if (pos >= arr.length)
                    break;
                val2 = arr[pos];
                pos++;
                if ((val2 & 0xC0) != 0x80)
                    break;
                if ((val0 & 0xF0) == 0xE0) {
                    ch = (((val0 & 0xF)<<12)  & 0x0000F000);
                    ch |= (((val1 & 0x3F)<<6) & 0x00000FC0);
                    ch |= (((val2 & 0x3F))    & 0x0000003F);
                }
                else if ((val0 & 0xF0) == 0xF0) {
                    if (pos >= arr.length)
                        break;
                    val3 = arr[pos];
                    pos++;
                    if ((val3 & 0xC0) != 0x80)
                        break;
                    ch = (((val0 & 0x7)<<18)   & 0x1C0000);
                    ch |= (((val1 & 0x3F)<<12) & 0x03F000);
                    ch |= (((val2 & 0x3F)<<6)  & 0x000FC0);
                    ch |= (((val3 & 0x3F))     & 0x00003F);
                }
                else {
                    break;
                }
            }
        }
        res.push(ch);
    }

    return String.fromCharCode.apply(this, res);
}

// NUIXE CHANGED: Only use the custom implementation of the base 64 encoder/decoder since atob and bto don't exist in NativeScript.
/* Convert a base64 string into an array of numeric byte values. */
/* No atob() in NativeScript, so we have to invent our own.
       This implementation is adapted from Parchment. */
var b64decoder = (function() {
    var b64encoder = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var out = [];
    var ix;
    for (ix=0; ix<b64encoder.length; ix++)
        out[b64encoder.charAt(ix)] = ix;
    return out;
})();

function decode_base64(base64data) {
    var out = [];
    var c1, c2, c3, e1, e2, e3, e4;
    var i = 0, len = base64data.length;
    while (i < len) {
        e1 = b64decoder[base64data.charAt(i++)];
        e2 = b64decoder[base64data.charAt(i++)];
        e3 = b64decoder[base64data.charAt(i++)];
        e4 = b64decoder[base64data.charAt(i++)];
        c1 = (e1 << 2) + (e2 >> 4);
        c2 = ((e2 & 15) << 4) + (e3 >> 2);
        c3 = ((e3 & 3) << 6) + e4;
        out.push(c1, c2, c3);
    }
    if (e4 == 64)
        out.pop();
    if (e3 == 64)
        out.pop();
    return out;
}

// NUIXE CHANGED: Same as above.
/* Convert an array of numeric byte values into a base64 string. */
function encode_base64(arr) {
    var coder = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var res = [];
    var byte0, byte1, byte2;
    for (var ix=0; ix<arr.length; ix += 3) {
        byte0 = arr[ix];
        byte1 = arr[ix+1];
        byte2 = arr[ix+2];
        res.push(coder.charAt((byte0 >> 2) & 0x3F));
        res.push(coder.charAt(((byte0 << 4) & 0x30) | ((byte1 >> 4) & 0x0F)));
        res.push(coder.charAt(((byte1 << 2) & 0x3C) | ((byte2 >> 6) & 0x03)));
        res.push(coder.charAt((byte2) & 0x3F));
    }
    if (byte1 === undefined && res.length >= 2) {
        res[res.length-2] = '=';
    }
    if (byte2 === undefined && res.length >= 1) {
        res[res.length-1] = '=';
    }
    return res.join('');
}

/* Given a PNG file, extract its dimensions. Return a {width,height}
   object, or undefined on error. 
*/
function find_dimensions_png(arr) {
    var pos = 0;
    if (arr[0] != 0x89 || String.fromCharCode.apply(this, arr.slice(1,4)) != 'PNG') {
        // NUIXE CHANGED: Replace GlkOte.log by the new qlog.
        qlog('find_dimensions_png: PNG signature does not match');
        return undefined;
    }
    pos += 8;
    while (pos < arr.length) {
        var chunklen = (arr[pos+0] << 24) | (arr[pos+1] << 16) | (arr[pos+2] << 8) | (arr[pos+3]);
        pos += 4;
        var chunktype = String.fromCharCode.apply(this, arr.slice(pos,pos+4));
        pos += 4;
        if (chunktype == 'IHDR') {
            var res = {};
            res.width  = (arr[pos+0] << 24) | (arr[pos+1] << 16) | (arr[pos+2] << 8) | (arr[pos+3]);
            pos += 4;
            res.height = (arr[pos+0] << 24) | (arr[pos+1] << 16) | (arr[pos+2] << 8) | (arr[pos+3]);
            pos += 4;
            return res;
        }
        pos += chunklen;
        pos += 4; /* skip CRC */
    }

    // NUIXE CHANGED: Replace GlkOte.log by the new qlog.
    qlog('find_dimensions_png: no PNG header block found');
    return undefined;
}

/* Given a JPEG file, extract its dimensions. Return a {width,height}
   object, or undefined on error. 
*/
function find_dimensions_jpeg(arr) {
    var pos = 0;
    while (pos < arr.length) {
        if (arr[pos] != 0xFF) {
            // NUIXE CHANGED: Replace GlkOte.log by the new qlog.
            qlog('find_dimensions_jpeg: marker is not 0xFF');
            return undefined;
        }
        while (arr[pos] == 0xFF) 
            pos += 1;
        var marker = arr[pos];
        pos += 1;
        if (marker == 0x01 || (marker >= 0xD0 && marker <= 0xD9)) {
            /* marker type has no data */
            continue;
        }
        var chunklen = (arr[pos+0] << 8) | (arr[pos+1]);
        if (marker >= 0xC0 && marker <= 0xCF && marker != 0xC8) {
            if (chunklen < 7) {
            // NUIXE CHANGED: Replace GlkOte.log by the new qlog.
                qlog('find_dimensions_jpeg: SOF block is too small');
                return undefined;
            }
            var res = {};
            res.height = (arr[pos+3] << 8) | (arr[pos+4]);
            res.width  = (arr[pos+5] << 8) | (arr[pos+6]);
            return res;
        }
        pos += chunklen;
    }

    // NUIXE CHANGED: Replace GlkOte.log by the new qlog.
    qlog('find_dimensions_jpeg: no SOF marker found');
    return undefined;
}

// NUIXE ADDED: An options parameter that will hold a reference to the GiLoad instance. (Since we can't make a reference to the closure from inside it, we'll have to pass it when calling the function.) It is also use for the update, error and warning functions.
/* Start the game (after de-blorbing, if necessary).
   This is invoked by whatever callback received the loaded game file.
*/
function start_game(image, options) {
    if (image.length == 0) {
        Glk.fatal_error("No game file was loaded. (Zero-length response.)");
        return;
    }

    // NUIXE TODO: Still useful?
    if (image[0] == 0x46 && image[1] == 0x4F && image[2] == 0x52 && image[3] == 0x4D) {
        var formtype = String.fromCharCode(image[8], image[9], image[10], image[11]);

        if (formtype == 'IFZS') {
            Glk.fatal_error("This is a saved-game file, not a "+all_options.game_format_name+" game file. You must launch the game first, then restore your save.");
            return;
        }

        if (formtype != 'IFRS') {
            Glk.fatal_error("This IFF file is not a Blorb file!");
            return;
        }

        if (all_options.blorb_gamechunk_type) {
            try {
                image = unpack_blorb(image, all_options.blorb_gamechunk_type);
            }
            catch (ex) {
                Glk.fatal_error("Blorb file could not be parsed: " + ex);
                return;
            }
        }
        if (!image) {
            Glk.fatal_error("Blorb file contains no "+all_options.game_format_name+" game!");
            return;
        }
    }

    // NUIXE REMOVED: Section that se the title of the webpage.

    // NUIXE CHANGED
    // TODO: Add autosave options and maybe others?
    VM.prepare(image, {
        glk: Glk,
        gi_dispa: GiDispa,
        gi_load: options.gi_load
    })

    // NUIXE CHANGED
    Object.assign(all_options)
    Glk.init({
        vm: VM,
        gi_dispa: GiDispa,
        gi_load: options.gi_load,
        exit_warning: all_options.exit_warning,
        nuixe: {
            on_update: options.on_update,
            on_error: options.on_error,
            on_warning: options.on_warning
        }
    });
}

/* End of GiLoad namespace function. Return the object which will
   become the GiLoad global. */
return {
    load_run: load_run,
    find_data_chunk: find_data_chunk,
    get_metadata: get_metadata,
    get_debug_info: get_debug_info,
    get_image_info: get_image_info,
    get_image_url: get_image_url
};

}
