import Quixe from "./quixe"
import Glk from "../glkote/glkapi"
import GiDispa from "./gi_dispa"
import GiLoad from "./gi_load"

export default class QuixeStory {
    /**
     * A wrapper to start an instance of a Quixe interpreter and interact with it.
     *
     * @param {string} base64tory - A string representing the story encoded in base 64.
     * @param {Object} options - The options needed to start the interpreter.
     * @param {Function} options.on_update - Function that will be called when Quixe wants to update the display. It take a single argument, an object representing a GlkOte update event.
     * @param {Function} options.on_error - Function that will be called when Quixe encounters an error. It take a string as argument.
     * @param {Function} options.on_warning - Function that will be called when the Quixe wants to update the display. It take a string as argument.
     */
    constructor(base64tory, options) {
        this._quixe = Quixe()
        this._glk = Glk()
        this._gi_dispa = GiDispa()
        this._gi_load = GiLoad()
        this._gi_load.load_run(base64tory, {
            vm: this._quixe,
            glk: this._glk,
            gi_dispa: this._gi_dispa,
            gi_load: this._gi_load,
            on_update: options.on_update,
            on_error: options.on_error,
            on_warning: options.on_warning
        })
    }

    /**
     * Send an input to the interpreter.
     *
     * @param {Object} glkEvent - The GlkOte input event to send to the interpreter.
     */
    send(glkEvent) {
        this._glk.accept(glkEvent)
    }
}
